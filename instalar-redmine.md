# Instalar Redmine local

## Clonar repo redmine

```sh
git clone git@github.com:redmine/redmine.git
git clone https://github.com/redmine/redmine.git
cd redmine
```

## MySQL

```sh
sudo mysql -u root -p
>password

mysql>
CREATE DATABASE redmine CHARACTER SET utf8mb4;
CREATE USER 'redmine'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON redmine.* TO 'redmine'@'localhost';

cp config/database.yml.example config/database.yml

nano config/database.yml
```

En este archivo copiamos

```yaml
production:
  adapter: mysql2
  database: redmine
  host: localhost
  port: 3307
  username: redmine
  password: "password"

```

Si da error mysql, puede que falte una dependencia

```sh
sudo apt-get install libmysqlclient-dev
```

## Bundler

```sh
gem install bundler

bundle config set --local without 'development test'

bundle exec rake generate_secret_token
```

## Migración

```sh
RAILS_ENV=production bundle exec rake db:migrate
```

> If you get this error with Ubuntu:

 ```sh
  Rake aborted!
  no such file to load -- net/https
  ```

> Then you need to install libopenssl-ruby1.8 just like this: apt-get install libopenssl-ruby1.8.

Cargar datos por defecto

```sh
RAILS_ENV=production REDMINE_LANG=es bundle exec rake redmine:load_default_data
```

Si no se especifica el lenguaje, promptea para preguntarlo

## Assets

```sh
mkdir -p tmp tmp/pdf public/plugin_assets
sudo chown -R lilith:lilith files log tmp public/plugin_assets
sudo chmod -R 755 files log tmp public/plugin_assets
```

## Iniciar servidor

Dos maneras:

```sh
bundle exec rails server webrick -e production

rails s -e production
```
