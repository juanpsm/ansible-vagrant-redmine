"""Role testing files using testinfra."""


def test_hosts_file(host):
    """Validate /etc/hosts file."""
    f = host.file("/etc/hosts")

    assert f.exists
    assert f.user == "root"
    assert f.group == "root"

def test_ruby(host, AnsibleVars):
    # PATH: "{{ rbenv_root }}/bin:{{ rbenv_root }}/shims:{{ rbenv_root }}/plugins/ruby-build/bin:{{ ansible_env.PATH }}"
    rbenv_root = AnsibleVars['rbenv_root']
    paths = (f"{rbenv_root}/bin", f"{rbenv_root}/shims", f"{rbenv_root}/plugins/ruby-build/bin")
    host.find_command("rbenv", extrapaths=paths)
    host.find_command("ruby", extrapaths=paths)
